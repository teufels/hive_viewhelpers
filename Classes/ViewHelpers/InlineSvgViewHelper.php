<?php

namespace HIVE\HiveViewhelpers\ViewHelpers;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use InvalidArgumentException;

/**
 * Class InlineSvgViewHelper.
 *
 * How to use:
 * {namespace hive=HIVE\HiveViewhelpers\ViewHelpers}
 * <f:format.raw>
 *  <hive:inlineSvg filePath="/absolute/path/to/some_file.svg" removeStyleAttributes="1" />
 *  </f:format.raw>
 */
class InlineSvgViewHelper extends AbstractViewHelper
{
    const SELECT_STYLE_ATTRIBUTE_REGEX = ['/(style=\".*?\")/'];

    public function initializeArguments()
    {
        $this->registerArgument('filePath', 'string', 'abosulte path to file', true);
        $this->registerArgument('removeStyleAttributes', 'boolean', 'remove Style Attributes', false, false);
        $this->registerArgument('additionalRemoveTagRegex', 'array', 'additional Remove Tag Regex', false, []);
    }

    /**
     * @return string
     */
    public function render()
    {
        $aArguments = $this->arguments;

        $absoluteFilePath = \TYPO3\CMS\Core\Core\Environment::getPublicPath() . $aArguments['filePath'];
        $removeStyleAttributes = $aArguments['removeStyleAttributes'];
        $additionalRemoveTagRegex =  $aArguments['additionalRemoveTagRegex'];

        if (!file_exists($absoluteFilePath)) {
            throw new InvalidArgumentException("file *$absoluteFilePath* does not exist on the server.");
        }
        $content = file_get_contents($absoluteFilePath);

        if ($removeStyleAttributes) {
            $regex = array_merge(self::SELECT_STYLE_ATTRIBUTE_REGEX, $additionalRemoveTagRegex);

            return preg_replace($regex, '', $content);
        }

        return  $content;
    }
}
